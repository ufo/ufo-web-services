#!/bin/sh
#
# changes-monitor:   The synchronization changes listener
#
# chkconfig: 345 85 15
# description:  This is a daemon which replicate the users database
#               and propage the changes in the different databases
#
# processname: changes-monitor
# pidfile: /var/run/changes-monitor.pid
#
### BEGIN INIT INFO
# Provides: changes-monitor
# Required-Start: $syslog $local_fs
# Required-Stop: $syslog $local_fs
# Default-Start: 3 4 5
# Default-Stop: 0 1 2 6
# Short-Description: The synchronization changes listener
# Description: This is a daemon which replicate the users database
#              and propage the changes in the different databases
### END INIT INFO

# Sanity checks.
[ -x /usr/sbin/ufo-changes-monitor ] || exit 0

# Source function library.
. /etc/rc.d/init.d/functions

# so we can rearrange this easily
processname=ufo-changes-monitor
servicename=ufo-changes-monitor

RETVAL=0

start() {
    echo -n $"Starting sync spawner daemon: "

    daemon --check $servicename $processname
    RETVAL=$?
    echo
    [ $RETVAL -eq 0 ] && touch /var/lock/subsys/$servicename
}

stop() {
    echo -n $"Stopping sync spawner daemon: "

    ## we don't want to kill all the per-user $processname, we want
    ## to use the pid file *only*; because we use the fake nonexistent 
    ## program name "$servicename" that should be safe-ish
    killproc $servicename -TERM
    RETVAL=$?
    echo
    if [ $RETVAL -eq 0 ]; then
        rm -f /var/lock/subsys/$servicename
        rm -f /var/run/changes-monitor.pid
    fi
}

# See how we were called.
case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    status)
        status $servicename
        RETVAL=$?
        ;;
    restart)
        stop
        start
        ;;
    condrestart)
        if [ -f /var/lock/subsys/$servicename ]; then
            stop
            start
        fi
        ;;
    reload)
        echo "Message bus can't reload its configuration, you have to restart it"
        RETVAL=$?
        ;;
    *)
        echo $"Usage: $0 {start|stop|status|restart|condrestart|reload}"
        ;;
esac
exit $RETVAL
