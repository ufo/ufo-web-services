"""Unit test for sync.py"""
#
# WARNING : YOU MUST PICK UP AN ADMIN NFS TICKET FIRST 
#          

import unittest
import os
import xmlrpclib as rpc

from ipalib.rpc import KerbTransport
from ipalib import api
from ipalib.errors import NotFound

import ufo.config as config
import ufo.utils as utils

from ufo.debugger import Debugger
from ufosync.sync import Sync

class CaseKnownInput(unittest.TestCase, Debugger):                   

    sync_remote = rpc.Server(config.sync_host, KerbTransport())
    storage_remote = rpc.Server(config.storage_host, KerbTransport())
    sync = Sync()
    try:
        meta = dict(apache_env=dict(KRB5CCNAME=os.environ["KRB5CCNAME"]))
    except KeyError:
        print "Error: you must export KRB5CCNAME first :"         
        print "export KRB5CCNAME=/path/to/you/ticket/cache"
        exit(1)

    def setUp(self):
        self.debug("Start")
        self.storage_directory = utils.random_string(dir='/home')
        self.uid = 0
        self.gid = 0
        self.debug("End")

    def tearDown(self):
        self.debug("Start")
        if (self.storage_remote.storage.is_dir(self.storage_directory)):
            # the storage_directory was created
            self.storage_remote.storage.rmdir(self.storage_directory, dict(force=True))
        self.debug("End")

    def test_init_storage_case(self):                        
        """ init_storage should create a storage directory on the Storage entity for 
        an user
        """
        self.debug("Start")
        self.sync.init_storage(self.meta, 
                               self.storage_directory, 
                               self.uid,
                               self.gid)

        # is the storage_directory created ?
        response = self.storage_remote.storage.is_dir(self.storage_directory)
        self.assertTrue(response)

        # the storage_directory has it the good permissions, the good uid 
        # and the gid ?
        response = self.storage_remote.storage.stat(self.storage_directory)
        self.assertEqual(self.uid, response['st_uid'])
        self.assertEqual(self.gid, response['st_gid'])

        # FIXME : controler les permissions aussi, voir response.st_mode
        # ...
        self.debug("End")

    def test_remove_storage_case(self):                        
        """ remove_storage should remove the storage_directory """
        self.debug("Start")
        # create the storage_directory on the Storage entity 
        self.storage_remote.storage.mkdir(self.storage_directory)
        # storage_directory should be created
        self.assertTrue(self.storage_remote.storage.is_dir(self.storage_directory))
        # delete the storage_directory
        self.sync.remove_storage(self.meta, self.storage_directory)
        # storage_directory should be deleted
        self.assertFalse(self.storage_remote.storage.is_dir(self.storage_directory))
        self.debug("End")

if __name__ == "__main__":
    unittest.main()   
