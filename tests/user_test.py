# WARNING : 
# YOU MUST PICK UP AN ADMIN NFS TICKET FIRST 
# DO NOT LOGIN AS root
#         

import unittest
import os
from ipalib import api
from ipalib.errors import NotFound

from ufoserver.user import (User, UserBadAttributeError, UserRequiredAttributeError, 
                            UserNoUserError)

from ufoserver import config

class CaseKnownInput(unittest.TestCase):                   
    user_name      = None
    first_name     = None
    last_name      = None
    realm          = None
    home_directory = None
    login_shell    = None
    user           = None
    mail           = None
    street         = None
    groups         = None

    try:
        meta = dict(apache_env=dict(KRB5CCNAME=os.environ["KRB5CCNAME"]))
    except KeyError:
        print "Error: you must export KRB5CCNAME first :"         
        print "export KRB5CCNAME=/path/to/you/ticket/cache"
        exit()
        
    # to use the ticket cache
    os.environ["KRB5CCNAME"]=meta['apache_env']['KRB5CCNAME']    
    
    try:
        api.bootstrap(context='webservices', in_tree=False, in_server=config.ufo_in_server)
    except StandardError:
        # the following exceptions can be ignored. That's no problem
        # API.bootstrap() already called
        pass
    try:
        api.finalize()
    except StandardError:
        # the following exceptions can be ignored. That's no problem
        # API.finalize() already called
        pass
    if ufo_in_server:
        try:
            api.Backend.ldap2.connect(ccache=api.Backend.krb.default_ccname())
        except StandardError:
            # this Exception can be ignored :
            # connect: 'context.xmlclient' already exists in thread 'MainThread'
            pass
    else:
        try:
            api.Backend.xmlclient.connect()
        except StandardError:
            # this Exception can be ignored :
            # connect: 'context.xmlclient' already exists in thread 'MainThread'
            pass
    
    def setUp(self):
        self.user_name      = 'user_test-user_name'
        self.first_name     = 'user_test-user_name-first_name'
        self.last_name      = 'user_test-user_name-last_name'
        self.realm          = config.realm
        self.home_directory = '/home/'+ self.user_name
        self.user           = User(self.meta, self.user_name)
        self.login_shell    = None
        self.mail           = None
        self.street         = None
        self.groups         = None

    def tearDown(self):
        # remove the temp user if it exists
        try:
            api.Command.user_show(unicode(self.user_name))            
            api.Command.user_del(unicode(self.user_name))
        except NotFound, e:
            if (e.errno == 4001):
                # user not found : no thing to do
                pass
            else:
                raise e
        except Exception:
            print "there was some problems but no problemous"
            pass
        
    def test_initialize_case(self):
        """ initialize should initialise the object user with the given parameters"""
        args = dict(first_name     = self.first_name,
                    last_name      = self.last_name,
                    home_directory = self.home_directory,
                    mail           = self.mail,
                    realm          = self.realm,
                    street         = self.street
                    )
        self.user.initialize(args)
        self.assertTrue(self.user.first_name, self.first_name)
        self.assertTrue(self.user.last_name, self.last_name)
        self.assertTrue(self.user.home_directory, self.home_directory)
        self.assertTrue(self.user.mail, self.mail)
        self.assertTrue(self.user.street, self.street)

        ## BAD INPUTS
        # FIXME create an other class test for bad inputs
        bad_args = dict(first_name     = "bad_first_name",
                        last_name      = "bad_last_name",
                        bad_attribute  = 'bad thing',
                        home_directory = "bad_home_directory"
                        )
        # # an exception should be raised
        self.assertRaises(UserBadAttributeError, 
                          self.user.initialize, 
                          bad_args)
        # the old attributes should not be modified
        self.assertTrue(self.user.first_name, self.first_name)
        self.assertTrue(self.user.last_name, self.last_name)
        self.assertTrue(self.user.home_directory, self.home_directory)
        
    def test_populate_case(self):
        """ populate should populate the object with the good informations"""
        # add a new user
        response = api.Command.user_add(unicode(self.user_name), 
                                        givenname=unicode(self.first_name), 
                                        sn=unicode(self.last_name), 
                                        gecos=unicode(self.user_name), 
                                        krbprincipalname=unicode(self.user_name +
                                                                 '@' + self.realm),
                                        all=True, raw=True)
        # populate
        response = self.user.populate()
        # tests
        self.assertTrue(self.user.user_name, response['user_name'])
        self.assertTrue(self.user.first_name, response['first_name'])
        self.assertTrue(self.user.last_name, response['last_name'])
        self.assertTrue(self.user.home_directory, response['home_directory'])
        self.assertTrue(self.user.first_name, self.first_name)
        self.assertTrue(self.user.last_name, self.last_name)
        self.assertTrue(self.user.home_directory, self.home_directory)
        self.assertTrue(self.user.groups, self.groups)
        

    def test_create_case(self):
        """ create should add a new user on the ldap """
        args = dict(first_name     = self.first_name,
                    last_name      = self.last_name,
                    home_directory = self.home_directory,
                    realm          = self.realm
                    )
        self.user.initialize(args)
        self.user.create()
        response = api.Command.user_show(unicode(self.user_name))
        self.assertTrue(self.first_name, response['result']['givenname'][0])
        self.assertTrue(self.last_name, response['result']['sn'][0])

    def test_delete_case(self):
        """ delete should delete a user from the ldap """
        # add a new user
        response = api.Command.user_add(unicode(self.user_name), 
                                        givenname=unicode(self.first_name), 
                                        sn=unicode(self.last_name), 
                                        gecos=unicode(self.user_name), 
                                        krbprincipalname=unicode(self.user_name +
                                                                 '@' + self.realm),
                                        all=True, raw=True)
        # this initialization is optional
        self.user = User(self.meta, self.user_name)
        args = dict(first_name     = self.first_name,
                    last_name      = self.last_name,
                    home_directory = self.home_directory,
                    realm          = self.realm
                    )
        self.user.initialize(args)
        # delete the user from ldap
        self.user.delete()
        # tests
        self.assertRaises(NotFound, api.Command.user_show, unicode(self.user_name))
        self.assertRaises(UserNoUserError, self.user.delete)
    def test_update_case(self):
        """ update should update a user attributes on the ldap """
        # add a new user
        response = api.Command.user_add(unicode(self.user_name), 
                                        givenname=unicode(self.first_name), 
                                        sn=unicode(self.last_name), 
                                        gecos=unicode(self.user_name), 
                                        krbprincipalname=unicode(self.user_name +
                                                                 '@' + self.realm),
                                        all=True, raw=True)
        # initialization
        new_first_name = "new_first_name"
        new_last_name  = "new_last_name"
        new_realm      = "new_realm"
        new_mail       = "my.mail@anywhere.com"
        new_street     = "overthere"
        new_home_directory = 'new home_directory'
        args = dict(first_name = new_first_name,
                    last_name  = new_last_name,
                    realm      = new_realm,
                    mail       = new_mail,
                    home_directory = new_home_directory,                   
                    street     = new_street
                    )        
        self.user.initialize(args)
        ## update
        self.user.update()

        ## tests
        # test if the attributes of user are updated. (this attribute 
        # are still in memory)
        self.assertTrue(new_first_name, self.user.first_name)
        self.assertTrue(new_last_name, self.user.last_name)
        self.assertTrue(new_home_directory, self.user.home_directory)
        self.assertTrue(new_mail, self.user.mail)
        self.assertTrue(new_street, self.user.street)

        # test if the ldap is updated
        response = api.Command.user_show(unicode(self.user_name))
        self.assertTrue(new_first_name, response['result']['givenname'][0])
        self.assertTrue(new_last_name, response['result']['sn'][0])

if __name__ == "__main__":
    unittest.main()   
