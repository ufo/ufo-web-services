"""Unit test for storage.py"""
#
# WARNING : YOU MUST PICK UP AN ADMIN NFS TICKET FIRST 
#          

import unittest
import os
import xmlrpclib as rpc
from ipalib.rpc import KerbTransport

from ufoaccount.account import Account, AccountAdmin
from ufoserver.user import User
from ufo.debugger import Debugger

import ufo.config as config
import ufo.utils as utils

class AccountCaseKnownInput(unittest.TestCase, Debugger):
    try:
        meta = dict(apache_env=dict(KRB5CCNAME=os.environ["KRB5CCNAME"]))
    except KeyError:
        print "Error: you must export KRB5CCNAME first :"         
        print "export KRB5CCNAME=/path/to/you/ticket/cache"
        exit(1)
        
    def setUp(self):
        self.account    = Account()
        self.user_name  = utils.random_string().lower()
        self.first_name = utils.random_string()
        self.last_name  = utils.random_string()
        self.realm      = config.realm
        
        self.user = User(self.meta, self.user_name)

    def tearDown(self):
        # delete the user
        try:
            self.user.delete()
        except:
            # no user was created
            pass

    def test_get_user_infos_case(self):                        
        """ get_user_infos should return all informations about the user """
        # create the user
        args = dict(first_name     = self.first_name,
                    last_name      = self.last_name,
                    realm          = self.realm
                    )
        self.user.initialize(args)
        self.user.create()
        # get the informations about the user
        response = self.account.get_user_infos(self.meta, self.user_name)
        self.assertTrue(response['user_name'], self.user_name)
        self.assertTrue(response['first_name'], self.first_name)
        self.assertTrue(response['last_name'], self.last_name)

    def test_set_user_infos_case(self):
        """ set_user_infos should change the informations about the user """
        # create the user
        args = dict(first_name     = self.first_name,
                    last_name      = self.last_name,
                    realm          = self.realm
                    )
        self.user.initialize(args)
        self.user.create()
        # Tests
        self.assertTrue(self.user.user_name, self.user_name)
        self.assertTrue(self.user.first_name, self.first_name)
        self.assertTrue(self.user.last_name, self.last_name)
        
        first_name = "new_first_name"
        last_name  = "new_last_name"
        realm      = "new_realm"
        mail       = "my.mail@anywhere.com"
        street     = "overthere"
        other = dict(first_name = first_name,
                     last_name  = last_name,
                     realm      = realm,
                     mail       = mail,
                     street     = street
                     )
        # update the user entries on ldap
        self.account.set_user_infos(self.meta, self.user_name, other)
        # Tests
        # update the user object
        self.user.populate()
        self.assertTrue(self.user.user_name, self.user_name)
        self.assertTrue(self.user.first_name, first_name)
        self.assertTrue(self.user.last_name, last_name)
        self.assertTrue(self.user.mail, mail)
        self.assertTrue(self.user.street, street)
        
class AccountAdminCaseKnownInput(unittest.TestCase, Debugger):
    try:
        meta = dict(apache_env=dict(KRB5CCNAME=os.environ["KRB5CCNAME"]))
    except KeyError:
        print "Error: you must export KRB5CCNAME first :"         
        print "export KRB5CCNAME=/path/to/you/ticket/cache"
        exit()

    def setUp(self):
        self.user_name = utils.random_string().lower()
        self.first_name = utils.random_string()
        self.last_name = utils.random_string()
        self.storage_directory = '/mnt/nfs/tests/' + self.user_name
        self.realm = config.realm
        self.mail = utils.random_string()+'@'+ utils.random_string()+'.com'
        self.street = utils.random_string()
        self.storage_remote = rpc.Server(config.storage_host, KerbTransport())
        self.account_admin = AccountAdmin() 
        self.user = User(self.meta, self.user_name)
        
    def tearDown(self):
        # delete the user
        try:
            self.user.delete()
        except:
            # no user was created
            print "Failed to delete user", self.user_name

        # delete the storage space if created
        try:
            self.storage_remote.storage.rmdir(self.storage_directory, 
                                              dict(force=True))
        except Exception, e :
            print 'Unknown exception :'
            print str(e)
        
    def test_create_user_case(self):                        
        """ create_user should create the user as well as his storage space """
        try:
            # create the user on ldap
            self.account_admin.create_user(self.meta, 
                                           self.user_name,
                                           self.first_name,
                                           self.last_name,
                                           self.realm,
                                           dict(mail=self.mail, street=self.street)
                                           )
            
            # is the user created on the ldap ?
            self.user.populate()
            self.assertTrue(self.user.user_name, self.user_name)
            self.assertTrue(self.user.first_name, self.first_name)
            self.assertTrue(self.user.last_name, self.last_name)
            self.assertTrue(self.user.mail, self.mail)
            self.assertTrue(self.user.street, self.street)
        
            # is the storage_directory created ?
            # get the path to the storage directory from the ldap
            self.storage_directory = self.user.home_directory
            response = self.storage_remote.storage.is_dir(self.storage_directory)
            self.assertTrue(response)
            
            # the storage_directory has it the good permissions, the good uid 
            # and the gid ?
            uid = self.user.uidnumber
            gid = self.user.gidnumber
            response = self.storage_remote.storage.stat(self.storage_directory)
            self.assertEqual(uid, response['st_uid'])
            self.assertEqual(gid, response['st_gid'])
        except:
            raise

    def test_create_user_with_defferent_home_directory_case(self):    
        """ create_user should create the user as well as his spicified storage space 
        """
        # create the user on ldap as well as his home storage
        self.account_admin.create_user(self.meta, 
                                       self.user_name,
                                       self.first_name,
                                       self.last_name,
                                       self.realm,
                                       dict(mail=self.mail, 
                                            street=self.street,
                                            home_directory=self.storage_directory)
                                       )

        # is the user created on the ldap ?
        self.user.populate()
        self.assertTrue(self.user.user_name, self.user_name)
        self.assertTrue(self.user.first_name, self.first_name)
        self.assertTrue(self.user.last_name, self.last_name)
        self.assertTrue(self.user.mail, self.mail)
        self.assertTrue(self.user.street, self.street)
        self.assertTrue(self.user.home_directory, self.storage_directory)
        
        # is the storage_directory created ?
        # get the path to the storage directory from the ldap
        response = self.storage_remote.storage.is_dir(self.storage_directory)
        self.assertTrue(response)

        # the storage_directory has it the good permissions, the good uid 
        # and the gid ?
        uid = self.user.uidnumber
        gid = self.user.gidnumber
        response = self.storage_remote.storage.stat(self.storage_directory)
        self.assertEqual(uid, response['st_uid'])
        self.assertEqual(gid, response['st_gid'])
                   
    def test_remove_user_case(self):
        """ remove_user should remove the user as well as his home directory"""
        # create the user to delete after
        self.account_admin.create_user(self.meta, 
                                       self.user_name,
                                       self.first_name,
                                       self.last_name,
                                       self.realm,
                                       dict(mail=self.mail, 
                                            street=self.street,
                                            home_directory=self.storage_directory)
                                       )
        # is the user created on the ldap ?
        self.user.populate()
        self.assertTrue(self.user.user_name, self.user_name)
        self.assertTrue(self.user.first_name, self.first_name)
        self.assertTrue(self.user.last_name, self.last_name)
        self.assertTrue(self.user.mail, self.mail)
        self.assertTrue(self.user.street, self.street)
        self.assertTrue(self.user.home_directory, self.storage_directory)

        # is the storage_directory created ?
        # get the path to the storage directory from the ldap
        self.storage_directory = self.user.home_directory
        response = self.storage_remote.storage.is_dir(self.storage_directory)
        self.assertTrue(response)

        # the storage_directory has it the good permissions, the good uid 
        # and the gid ?
        uid = self.user.uidnumber
        gid = self.user.gidnumber
        response = self.storage_remote.storage.stat(self.storage_directory)
        self.assertEqual(uid, response['st_uid'])
        self.assertEqual(gid, response['st_gid'])

        # delete the user from the ldap
        self.account_admin.remove_user(self.meta, self.user_name)
        # TESTS
        # is the storage_directory deleted ?
        response = self.storage_remote.storage.is_dir(self.storage_directory)
        self.assertFalse(response)
        # is the user deleted ?
        self.assertRaises(Exception, self.user.populate)

if __name__ == "__main__":
    unittest.main()   
