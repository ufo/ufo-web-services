# Copyright (C) 2010  Agorabox. All Rights Reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""Unit test for storage.py"""

import unittest
import os
import xmlrpclib as rpc
from ipalib.rpc import KerbTransport

import ufo.config as config


class CaseKnownInput(unittest.TestCase):                   

    def setUp(self):
        self.storage_remote = rpc.Server(config.storage_host,
                                         KerbTransport())
        # self.temp_dir_path = '/tmp/test_storage-my_tmp_file'
        self.temp_dir_path = '/mnt/nfs/tests/storage_dir'
        self.temp_file_path = None
        
        # self.dir_temp       = '/mnt/nfs/tests'
        self.existing_dir       = '/mnt/nfs/tests'
        self.uid            = 0
        self.gid            = 0

    def tearDown(self):
        if (self.temp_file_path and 
            (self.storage_remote.storage.is_file(self.temp_file_path))):
            self.storage_remote.storage.rmfile(self.temp_file_path)
        if (self.temp_dir_path and 
            (self.storage_remote.storage.is_dir(self.temp_dir_path))):
            self.storage_remote.storage.rmdir(self.temp_dir_path)                

    def test_mkdir_case(self):                        
        """mkdir should create the directory path_directory"""  
        self.storage_remote.storage.mkdir(self.temp_dir_path)
        response = self.storage_remote.storage.is_dir(self.temp_dir_path)
        self.assertTrue(response)

    def test_chown_case(self):
        """chown should change the owner of the directory or the file """
        self.storage_remote.storage.mkdir(self.temp_dir_path)
        self.storage_remote.storage.chown(self.temp_dir_path, self.uid, self.gid)
        statinfo = self.storage_remote.storage.stat(self.temp_dir_path)
        self.assertEqual(statinfo['st_uid'], self.uid)
        self.assertEqual(statinfo['st_gid'], self.gid)
        
    def test_rmdir(self):
        """ rmdir should delete a directory even if it is not empty """    
        self.storage_remote.storage.mkdir(self.temp_dir_path)
        self.storage_remote.storage.rmdir(self.temp_dir_path)
        self.assertFalse(self.storage_remote.storage.is_dir(self.temp_dir_path))

    # FIXME : find how to tests this method
    # def test_rmfile(self):
    #     """ rmfile should delete the file passed as argument """
    #     self.temp_file_path = tempfile.mkstemp(dir=self.dir_temp)[1]
    #     self.storage_remote.storage.rmfile(self.temp_file_path)
    #     self.assertFalse(self.storage_remote.storage.is_file(self.temp_file_path))
        
    def test_is_dir(self):
        """ is_dir verify that path_object is a directory """
        response = self.storage_remote.storage.is_dir(self.existing_dir)
        self.assertTrue(response)

    # FIXME : find how to tests this method
    # def test_is_file(self):
    #     """ is_file verify that path_object is a file """
    #     self.temp_file_path = tempfile.mkstemp(dir=self.dir_temp)[1]
    #     response = self.storage_remote.storage.is_file(self.temp_file_path)
    #     self.assertTrue(response)

    def test_stat(self):
        """ stat return an object whose attributes correspond to the members 
        of the stat structure
        """
        self.storage_remote.storage.mkdir(self.temp_dir_path)
        response = self.storage_remote.storage.stat(self.temp_dir_path)
        self.assertEqual(response['st_uid'], self.uid)
        self.assertEqual(response['st_gid'], self.gid)
        # FIXME : how to test this method ?
        # self.assertEqual(response['st_mode'], os.stat(self.temp_file_path).st_mode)

if __name__ == "__main__":
    unittest.main()   
