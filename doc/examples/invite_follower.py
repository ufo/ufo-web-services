"""

"""
import sys

from ufo.config import account_host

import xmlrpclib as rpc
from ipalib.rpc import KerbTransport

from ufo.config import account_host


if sys.argv.__len__() != 2:
    print """ 
usage :
  %s new_follower
"""% sys.argv[0]
    exit(1)

account_remote = rpc.Server(account_host, KerbTransport())

new_follower = unicode(sys.argv[1], "utf8")

print ""
print "---> inviting '%s' by to be follower... " % new_follower
account_remote.account.invite_new_follower(new_follower)
print "[OK]"

