#!/usr/bin/env python

from ufo.config import account_host

import xmlrpclib as rpc
from ipalib.rpc import KerbTransport

from ufo.config import account_host, realm 

# utils 
def random_string(dir=None):
    '''
    Returns a random string with 10 chars
    Example : zwEeymTWdh
    '''
    import random, string
    random.seed()
    d = [random.choice(string.letters) for x in xrange(10)]
    if (dir == None):
        return "".join(d)
    else:
        # if dir='/tmp///' we return some thing like '/tmp/zwEeymTWdh'
        return (str(dir).rstrip('/') +
                '/' +
                ''.join(d)
                )

account_remote = rpc.Server(account_host, KerbTransport())

user_name  = random_string()
first_name = random_string()
last_name  = random_string()
#realm      = 'GAMMA.AGORABOX.ORG'
mail       = user_name + '@domain.com'
street     = 'Street' + random_string()

print "Creating a new user with "
print "user_name : %s"% user_name
print "first_name : %s"% first_name
print "last_name : %s"% last_name
print "realm : %s"% realm
print "mail : %s"% mail
print "street : %s"% street
print "..."

account_remote.account_admin.create_user(user_name,
                                         first_name,
                                         last_name,
                                         realm,
                                         dict(mail=mail, street=street)
                                         )

print "The user %s is created : "% user_name
print account_remote.account.get_user_infos(user_name)

print "Deleting the user %s ... " % user_name
account_remote.account_admin.remove_user(user_name)
print "Ok"










