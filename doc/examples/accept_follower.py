"""

"""
import sys

from ufo.config import account_host

import xmlrpclib as rpc
from ipalib.rpc import KerbTransport

from ufo.config import account_host



if sys.argv.__len__() != 2:
    print """ 
usage :
  %s pending_follower
"""% sys.argv[0]
    exit(1)

account_remote = rpc.Server(account_host, KerbTransport())

pending_follower = unicode(sys.argv[1], "utf8")

print ""
print "---> accepting '%s' as follower... " % pending_follower
account_remote.account.accept_follower(pending_follower)
print "[OK]"

