"""
Delete a follower from the friends list of the specefied user
"""
import sys

from ufo.config import account_host

import xmlrpclib as rpc
from ipalib.rpc import KerbTransport

from ufo.config import account_host


if sys.argv.__len__() != 2:
    print """ 
usage :
  %s follower_to_delete
"""% sys.argv[0]
    exit(1)

account_remote = rpc.Server(account_host, KerbTransport())

follower_to_delete = unicode(sys.argv[1], "utf8")

print ""
print "---> deleting '%s' from the friends list..." % follower_to_delete
account_remote.account.remove_follower(follower_to_delete)
print "[OK]"

