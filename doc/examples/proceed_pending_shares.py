"""

"""
import os
import sys

from ufo.config import sync_host

import xmlrpclib as rpc
from ipalib.rpc import KerbTransport


if sys.argv.__len__() != 2:
    print """ 
usage :
  %s participant
"""% sys.argv[0]
    exit(1)

sync_remote = rpc.Server(sync_host, KerbTransport())

participant = sys.argv[1]

print ""
print "---> proceed pending shares for %s" % participant

sync_remote.sync.proceed_pending_shares(participant)
