"""

"""
import os
import sys

from ufo.config import sync_host

import xmlrpclib as rpc
from ipalib.rpc import KerbTransport

if sys.argv.__len__() != 3:
    print """ 
usage :
  %s participant_to_delete document_id
"""% sys.argv[0]
    exit(1)

sync_remote = rpc.Server(sync_host, KerbTransport())

participant_to_delete = unicode(sys.argv[1], "utf8")
document_id = unicode(sys.argv[2], "utf8")

print ""
print ("---> deleting '%s' from the participants list of '%s'... " % 
       (participant_to_delete, document_id))
sync_remote.sync.remove_participant_from_share(participant_to_delete, document_id)
print "[OK]"
print ""
