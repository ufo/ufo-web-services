"""

"""
import os
import sys

from ufo.config import sync_host

import xmlrpclib as rpc
from ipalib.rpc import KerbTransport


if sys.argv.__len__() != 3:
    print """ 
usage :
  %s new_participant document_id
"""% sys.argv[0]
    exit(1)

sync_remote = rpc.Server(sync_host, KerbTransport())

new_participant = sys.argv[1]
document_id = sys.argv[2]

print "Sharing '", document_id, "'with", new_participant

sync_remote.sync.add_new_share(new_participant, document_id, "R")

