"""

"""
import sys

from ufo.config import account_host

import xmlrpclib as rpc
from ipalib.rpc import KerbTransport


if sys.argv.__len__() != 2:
    print """ 
usage :
  %s pending_following
"""% sys.argv[0]
    exit(1)

account_remote = rpc.Server(account_host, KerbTransport())

pending_following = unicode(sys.argv[1], "utf8")

print ""
print "---> accepting '%s' as follower... " % pending_following
account_remote.account.accept_following(pending_following)
print "[OK]"

