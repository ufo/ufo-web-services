# -*- coding: utf-8 -*-
"""

"""
import os
import sys

from ufo.config import sync_host

import xmlrpclib as rpc
from ipalib.rpc import KerbTransport


if sys.argv.__len__() != 2:
    print """ 
usage :
  %s document_id
"""% sys.argv[0]
    exit(1)

sync_remote = rpc.Server(sync_host, KerbTransport())

document_id = unicode(sys.argv[1], "utf8")

print "---> list of participants of '%s' :" % document_id

shares = sync_remote.sync.list_participants_of_shared_doc(document_id)
if shares:
    print "provider\tparticipant\tdocument\tpermissions\tflags"
    print "========\t===========\t========\t===========\t====="
else:
    print "[ No shares ]"
    print ""
    exit(0)
for share in shares:
    print ("%s\t%s\t%s\t%s\t%s" % (share["provider"], share["participant"], 
                                   share["fileid"], share["permissions"], 
                                   share["flags"]))
print ""
