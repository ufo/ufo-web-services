"""

"""
import sys

from ufo.config import account_host

import xmlrpclib as rpc
from ipalib.rpc import KerbTransport

from ufo.config import account_host


if sys.argv.__len__() != 2:
    print """ 
usage :
  %s new_following
"""% sys.argv[0]
    exit(1)

account_remote = rpc.Server(account_host, KerbTransport())

new_following = unicode(sys.argv[1], "utf8")

print ""
print "---> inviting '%s' to be a following... " % new_following
account_remote.account.invite_new_following(new_following)
print "[OK]"

