import os
from time import sleep
from multiprocessing import current_process, log_to_stderr, Process
from multiprocessing.managers import BaseManager
import subprocess
import random
import string
import pwd
from ufo.filesystem import CouchedFileSystem, CouchedFile
import logging
from ufoserver import config
import base64
import random
from ufo.utils import ComponentProxy

logger = log_to_stderr()
logger.setLevel(logging.DEBUG)

BASE_PORT = 50300
BASE_ADDR = "127.0.0.1"

class RemoteCouchedFileSystem(CouchedFileSystem):
    word = ""
    krb5ccname = ""

    def __init__(self, remote_user):
        CouchedFileSystem.__init__(self, config.fs_mount_point, remote_user.split('@')[0], config.couchdb_host)

    def get_credentials(self, remote_user, krb5ccname):
        self.remote_user = remote_user
        pw = pwd.getpwnam(remote_user)
        self.uid = pw[2]
        os.setuid(self.uid)

        RemoteCouchedFileSystem.krb5ccname = "/tmp/krb5cc_%s_%s" % (self.uid, base64.b64encode("%s" % random.randint(pow(2, 32), pow(2, 128))))
        os.environ["KRB5CCNAME"] = self.krb5ccname

        f = open(self.krb5ccname, "wb")
        f.write(krb5ccname)
        f.close()
        os.system("chmod og-rwx %s" % self.krb5ccname)

        sleep(1)

    def __del__(self):
        pass
        # if os.path.exists(self.krb5ccname):
        #     os.unlink(self.krb5ccname)

    def listdir(self, path):
        results = list(CouchedFileSystem.listdir(self, path))
        return results

    def readfile(self, path):
        f = self.open(path, os.O_RDONLY)
        content = f.read()
        return content

    def writefile(self, path, flags, uid, gid, mode, content):
        f = self.open(path, flags, uid, gid, mode)
        f.write(content)
        f.close()

    def share_file(self, buddy, docid, mode):
        meta = { "apache_env" : { "KRB5CCNAME" : self.krb5ccname } }
        remote_sync = ComponentProxy("ufosync.sync.Sync", config.sync_host, meta = meta)
        remote_sync.add_new_share(buddy, docid, mode)

    def unshare_file(self, buddy, docid):
        meta = { "apache_env" : { "KRB5CCNAME" : self.krb5ccname } }
        remote_sync = ComponentProxy("ufosync.sync.Sync", config.sync_host, meta = meta)
        remote_sync.remove_participant_from_share(buddy, docid)

    def get(self, path):
        return self._get(path)

class SyncManager(BaseManager):
    pass

class ProcessSpawner(object):
    procs = {}
    procsInfos = {}

    def __init__(self):
        self.procs = {}
        self.procsInfos = {}

    def list(self):
        return self.procsInfos

    def getProcInfosFor(self, user, krb5ccname):
        if not user in self.procsInfos:
            port = self.findPort()
            pw = self.genPassword()

            addr=(BASE_ADDR, port)
            logger.warning("Creating SyncManager:%s at port %d with authkey %s" % (user, port, pw))
            obj = RemoteCouchedFileSystem(user)
            SyncManager.register("RemoteCouchedFileSystem", lambda: obj)
            def create_couched_file(path, flags, uid, gid, mode):
                file = CouchedFile(path, flags, uid, gid, mode, obj)
                file.write = file.write
                return file
            SyncManager.register("CouchedFile", create_couched_file)
            self.procs[port] = SyncManager(address=addr, authkey=pw)
            self.procs[port].start(initializer=obj.get_credentials, initargs = (user, krb5ccname))
            self.procsInfos[user] = {"address" : addr, "authkey" : pw}
            
        return self.procsInfos[user]
    
    def genPassword(self, size=14):
        return ''.join([random.choice(''.join([string.digits, string.letters, '_'])) for i in range(0, size)])

    def findPort(self, start=BASE_PORT):
        lowestFreePort = start
        if not len(self.procs) == 0:
            for testedPort in sorted(self.procs):
                if testedPort == lowestFreePort:
                    lowestFreePort += 1

        return lowestFreePort

process = ProcessSpawner()

BaseManager.register("get_process", callable=lambda:process)
