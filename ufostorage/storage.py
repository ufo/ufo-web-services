# Copyright (C) 2010  Agorabox. All Rights Reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import os
import krbV
import time
import tempfile
import nfs4acl
import stat
import errno

from ufoserver import config
from ufo import utils
from ufo.utils import krb5principal
from ufo.database import DocumentHelper, DocumentException
from ufo.filesystem import SyncDocument, CouchedFileSystem
from ufo.constants import FriendshipStatus, ShareDoc
from ufo.debugger import Debugger
from ufo.sharing import ShareDocument
from ufo.errors import *
from ufoserver.user import *

from multiprocessing.managers import BaseManager


class Storage(Debugger):
    def __init__(self):
        """
        initiate the environment from which our methods will be called. 
        """
        pass

    def _check_permission(self, meta):
        """
        Check if the principal calling this component has the good 
        permissions.
        The principal must be member of the ufoadmins to work on the 
        storage entity.
        """
        self.debug('Start')

        krbccache = os.environ["KRB5CCNAME"] = meta['apache_env']['KRB5CCNAME']
        try:
            principal = utils.get_current_principal(krbccache)
            self.debug("principal: %s" % principal)

            principal_name = principal.split('@')[0]
            self.debug("principal_name: %s" % principal_name)

            if user_member_of(meta, 
                              principal_name, 
                              config.storage_admins_group):
                return True

            else:
                raise BadComponentPerms()

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

        finally:
            self.debug('End')

    def _build_ticket(self, meta):
        """
        Build a kerberos ticket from existing keytab.
        @param meta : Contain all meta-datas like the env variables
        @return : Path to the cache file containing the new ticket
        """
        try:
            self.debug('Start')

            # check if the caller has correct permissions
            os.environ["KRB5CCNAME"] = meta['apache_env']['KRB5CCNAME']
            self.debug("KRB5CCNAME => %s" % meta['apache_env']['KRB5CCNAME'])

            try:
                self._check_permission(meta)
            except Exception, e:
                self.debug('Exception raised : %s' % str(e))
                raise e

            time.sleep(1) ## first sleep

            # Create a new credentials cache for this Apache process fetched
            # from ipaserver/plugins/ldap2.py without this prefix it do not work 
            cccahe_file = tempfile.mkstemp(dir='/tmp', prefix='krb5cc_ufo_')[1]

            principal  = config.nfsadmin_principal
            krbcontext = krbV.default_context()
            keytab     = krbV.Keytab(name=config.nfs_admin_keytab,
                                     context=krbcontext)
            principal  = krbV.Principal(name=principal, context=krbcontext)

            os.environ['KRB5CCNAME'] = cccahe_file
            ccache = krbV.CCache(name=os.environ['KRB5CCNAME'],
                                 context=krbcontext,
                                 primary_principal=principal)

            ccache.init(principal)
            ccache.init_creds_keytab(keytab=keytab, principal=principal)

            time.sleep(1) ## second sleep

            return os.environ['KRB5CCNAME']

        finally:
            self.debug('End')

    def mk_home_dir(self, meta, user_name):
        """
        create a new directory at path_directory
        """
        self.debug('Start with user: %s' % user_name)

        try:
            homedirpath = os.path.join(os.sep, user_name)
            ccache_file = None
            os.environ["KRB5CCNAME"] = meta['apache_env']['KRB5CCNAME']
            self.debug("KRB5CCNAME => %s" % meta['apache_env']['KRB5CCNAME'])

            ccache_file = self._build_ticket(meta)

            # Filesystem helper to manager user files
            couchfs = CouchedFileSystem(config.fs_mount_point, user_name)

            # Get all informations about participant
            user = User(meta, user_name)
            user.populate()

            self.debug("Create home directory of %s, mode: 0700, uid: %s, gid: %s"
                       % (user_name, user.uidnumber, user.gidnumber))

            # Create the directory with the read, write and execution 
            # permissions, for the owner.
            # Other users do not have any permissions on this directory.
            # this method can raise the OSError Exception.

            couchfs.mkdir(homedirpath, 0701)
            couchfs.chown(homedirpath, user.uidnumber, user.gidnumber)

        except Exception, e:
            self.debug("%s directory do not was created." % homedirpath)

            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

        finally:           
            if ccache_file: os.remove(ccache_file)
            os.environ["KRB5CCNAME"] = meta['apache_env']['KRB5CCNAME']

            self.debug('End')

    def rm_home_dir(self, meta, user_name, force=False):
        """
        delete a directory even if it is not empty
        if force is True, the errors will be ignore
        """
        self.debug('Start with user_name: %s, force: %s' % (user_name, str(force)))

        try:
            ccache_file = None
            os.environ["KRB5CCNAME"] = meta['apache_env']['KRB5CCNAME']
            self.debug("KRB5CCNAME => %s" % meta['apache_env']['KRB5CCNAME'])

            ccache_file = self._build_ticket(meta)

            # Filesystem helper to manager user files
            couchfs = CouchedFileSystem(config.fs_mount_point, user_name)
            couchfs.rmdir(os.path.join(os.sep, user_name), force=force)

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

        finally:
            if ccache_file: os.remove(ccache_file)
            os.environ["KRB5CCNAME"] = meta['apache_env']['KRB5CCNAME']

            self.debug('End')

    def get_quota(self, meta, user_name):
        """
        Set a quota for the specified user by calling
        the set_quota method of quota daemon
        """
        self.debug('Getting quota for user %s' % user_name)

        try:
            ccache_file = None
            os.environ["KRB5CCNAME"] = meta['apache_env']['KRB5CCNAME']
            self.debug("KRB5CCNAME => %s" % meta['apache_env']['KRB5CCNAME'])

            class QuotaManager(BaseManager): pass
            QuotaManager.register('quotad')
            manager = QuotaManager(address=('', config.quota_daemon_port), authkey=config.quota_daemon_authkey)
            manager.connect()
            quotad = manager.quotad()
            return quotad.get_quota(open(os.environ["KRB5CCNAME"], 'r').read(), user_name)

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

        finally:
            self.debug('End')


    def set_quota(self, meta, user_name, quota):
        """
        Set a quota for the specified user by calling
        the set_quota method of quota daemon
        """
        self.debug('Setting quota %d for user %s' % (quota, user_name))

        try:
            ccache_file = None
            os.environ["KRB5CCNAME"] = meta['apache_env']['KRB5CCNAME']
            self.debug("KRB5CCNAME => %s" % meta['apache_env']['KRB5CCNAME'])

            ccache_file = self._build_ticket(meta)

            class QuotaManager(BaseManager): pass
            QuotaManager.register('quotad')
            manager = QuotaManager(address=('', config.quota_daemon_port), authkey=config.quota_daemon_authkey)
            manager.connect()
            quotad = manager.quotad()
            quotad.set_quota(open(os.environ["KRB5CCNAME"], 'r').read(), user_name, quota)

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

        finally:
            if ccache_file: os.remove(ccache_file)
            os.environ["KRB5CCNAME"] = meta['apache_env']['KRB5CCNAME']

            self.debug('End')

# Useless and dangerous.
#
#    def chown(self, meta, filepath, uid, gid):
#        """
#        Change the owner of the object located at filepath, the new owner is 
#        member of 'gid' group and has 'uid' as user identifier.
#        Note : this method is correctely executed only when you 
#        are a root user, or when you are writing in the NFS directory 
#        with the correct ticket (the principal mapped as a root user on 
#        your NFS file system. See /etc/idmapd.conf)
#        """
#        self.debug('Start with filepath:%s, uid:%i, gid:%i' %
#                    filepath, uid, gid)
#
#        try:
#            ccache_file = None
#            os.environ["KRB5CCNAME"] = meta['apache_env']['KRB5CCNAME']
#            self.debug("KRB5CCNAME => %s" % meta['apache_env']['KRB5CCNAME'])
#
#            ccache_file = self._build_ticket(meta)
#            os.chown(filepath, uid, gid)
#
#            self.debug("%s ownership change. New owner uid = %d and gid = %d" %
#                        filepath, uid, gid)
#
#        except Exception, e:
#            if isinstance(e, PrivateError):
#                self.debug("Raising exception: %s, %s" % (type(e), e))
#            else:
#                self.debug_exception()
#
#            raise
#
#        finally:
#            if ccache_file: os.remove(ccache_file)
#            os.environ["KRB5CCNAME"] = meta['apache_env']['KRB5CCNAME']
#
#            self.debug('End')

# Useless and dangerous.
#
#    def rmfile(self, meta, filepath, force=False):
#        """
#        Delete the file pointed by 'filepath'.
#        """
#        self.debug('Start with filepath: %s, force: %s' % (filepath, str(force)))
#        try:
#            ccache_file = None
#            os.environ["KRB5CCNAME"] = meta['apache_env']['KRB5CCNAME']
#            self.debug("KRB5CCNAME => %s" % meta['apache_env']['KRB5CCNAME'])
#
#            ccache_file = self._build_ticket(meta)            
#            os.remove(filepath)
#
#            self.debug("%s was removed correctely" % filepath)
#
#        except Exception, e:
#            if isinstance(e, PrivateError):
#                self.debug("Raising exception: %s, %s" % (type(e), e))
#            else:
#                self.debug_exception()
#
#            raise
#
#        finally:           
#            if ccache_file: os.remove(ccache_file)
#            os.environ["KRB5CCNAME"] = meta['apache_env']['KRB5CCNAME']
#
#            self.debug('End')

# Available via CouchedFileSytem api.
#
#    def is_dir(self, meta, filepath):
#        """
#        verify that filepath is a directory
#        return True if it is, False else
#        """
#        self.debug('Start with filepath: %s' % filepath)
#
#        try:
#            ccache_file = None
#            os.environ["KRB5CCNAME"] = meta['apache_env']['KRB5CCNAME']
#            self.debug("KRB5CCNAME => %s" % meta['apache_env']['KRB5CCNAME'])
#
#            ccache_file = self._build_ticket(meta)
#            response = os.path.isdir(filepath)
#
#            self.debug("is_dir(%s) returns %s as response" % (filepath, response))
#            return response
#
#        except Exception, e:
#            if isinstance(e, PrivateError):
#                self.debug("Raising exception: %s, %s" % (type(e), e))
#            else:
#                self.debug_exception()
#
#            raise
#
#        finally:           
#            if ccache_file: os.remove(ccache_file)
#            os.environ["KRB5CCNAME"] = meta['apache_env']['KRB5CCNAME']
#
#            self.debug('End')

# Available via CouchedFileSytem api.
#
#    def is_file(self, meta, filepath):
#        """
#        Verify that filepath is a file.
#
#        Return: True if it is, False else
#        """
#        self.debug('Start with filepath: %s' % filepath)
#
#        try:
#            ccache_file = None
#            os.environ["KRB5CCNAME"] = meta['apache_env']['KRB5CCNAME']
#            self.debug("KRB5CCNAME => %s" % meta['apache_env']['KRB5CCNAME'])
#
#            ccache_file = self._build_ticket(meta)
#            response = os.path.isfile(filepath)
#
#            self.debug("is_file(%s) returns %s as response" % (filepath, response))
#            return response
#
#        except Exception, e:
#            if isinstance(e, PrivateError):
#                self.debug("Raising exception: %s, %s" % (type(e), e))
#            else:
#                self.debug_exception()
#
#            raise
#
#        finally:           
#            if ccache_file: os.remove(ccache_file)
#            os.environ["KRB5CCNAME"] = meta['apache_env']['KRB5CCNAME']
#
#            self.debug('End')

# Available via CouchedFileSytem api.
#
#    def stat(self, meta, filepath):
#        """
#        Perform a stat() system call on the given path and return a 
#        dictionnary object and not a posix.stat_result object.
#        See http://docs.python.org/library/os.html#os.stat for further 
#        informations on posix.stat_result
#        The dictionnary returned is : 
#        {'st_atime': 1274099298,
#        'st_ctime': 1274273727,
#        'st_dev': 64768L,
#        'st_gid': 0,
#        'st_ino': 32130L,
#        'st_mode': 17407,
#        'st_mtime': 1274273727,
#        'st_nlink': 3,
#        'st_size': 4096L,
#        'st_uid': 0}
#        """
#        self.debug(('Start with filepath: %s')%
#                    (filepath))
#        try:
#            ccache_file = None
#            os.environ["KRB5CCNAME"]=meta['apache_env']['KRB5CCNAME']
#            ccache_file = self._build_ticket(meta)            
#            response = os.stat(filepath)
#
#            self.debug(("stat(%s) returns %s as response") %
#                        (filepath, str(response)))
#
#            return { 'st_mode'  : response.st_mode,
#                     'st_ino'   : response.st_ino,
#                     'st_dev'   : response.st_dev,
#                     'st_nlink' : response.st_nlink,
#                     'st_uid'   : response.st_uid,
#                     'st_gid'   : response.st_gid,
#                     'st_size'  : response.st_size,
#                     'st_atime' : response.st_atime,
#                     'st_mtime' : response.st_mtime,
#                     'st_ctime' : response.st_ctime }
#
#        except Exception, e:
#            if isinstance(e, PrivateError):
#                self.debug("Raising exception: %s, %s" % (type(e), e))
#            else:
#                self.debug_exception()
#
#            raise
#
#        finally:
#            if ccache_file: 
#              os.remove(ccache_file)
#            os.environ["KRB5CCNAME"]=meta['apache_env']['KRB5CCNAME']            
#            self.debug('End')


###############################################################################
# Access without an nfsadmin ticket. Each user has access to his files.
###############################################################################

    @krb5principal
    def is_owner(self, meta, principal, fileid):
        """
        verify that filepath is owned by user
        return True if it is, False else
        """

        try:
            self.debug('Start with printcipal: %s' % principal)

            document = DocumentHelper(SyncDocument, principal).by_id(key=fileid, pk=True)
            filepath = os.path.join(document.dirpath, document.filename.decode('utf-8'))
            realpath = os.path.join(config.fs_mount_point, filepath[1:])

            self.debug('User: %s, filepath: %s' % (principal, realpath.encode('utf-8')))

            user = User(meta, principal)
            user.populate()

            return (user.uidnumber == os.stat(realpath.encode('utf-8')).st_uid
                    and user.uidnumber == document.uid)

        except (OSError, PrivateError), e:
            if isinstance(e, OSError) and e.errno == errno.EACCES:
              return False

            self.debug("Raising exception: %s, %s" % (type(e), e))
            raise

        except Exception, e:
            self.debug_exception()
            raise

        finally:           
            self.debug('End')

    @krb5principal
    def add_permissions(self, meta, principal, participant, fileid, new_permissions):
        """
        add new permissions on the filepath for the participant
        @param owner       : the owner of the document refered by filepath
        @param participant : user who receive the new permissions
        @param filepath    : document
        @new_permissions   : "R" or "RW" 
        """

        try:
            self.debug('Start')

            doc_helper = DocumentHelper(SyncDocument, principal)
            document = doc_helper.by_id(key=fileid, pk=True)
            filepath = os.path.join(document.dirpath, document.filename.decode('utf-8'))

            self.debug('Owner: %s, participant: %s, filepath: %s, new_permissions: %s'
                       % (principal, participant, filepath.encode('utf-8'), new_permissions))

            if not self.is_owner(meta, fileid):
                raise BadOwnerError()

            realpath = os.path.join(config.fs_mount_point, filepath[1:])

            # Read permissions
            if new_permissions == ShareDoc.R_FLAG:
                # get all informations about participant
                user = User(meta, participant)
                user.populate()

                # using ACL nfsv4 to add R/W permissions for participant
                # ace_string is some thing like "A::raca@gamma.agorabox.org:R"
                ace_string = "A::%s@%s:R" % (participant, user.realm.lower())
                self.debug("ace_string : %s" % ace_string)

                nfs4acl.ace_add(ace_string, realpath.encode('utf-8'))

            # Read and write permissions
            elif new_permissions == ShareDoc.RW_FLAG:
                # get all informations about participant
                user = User(meta, participant)
                user.populate()                
                # using ACL nfsv4 to add R/W permissions for participant
                # ace_string is some thing like "A::raca@gamma.agorabox.org:RW"
                ace_string = "A::%s@%s:RW" % (participant, user.realm.lower())
                self.debug("ace_string : %s" % ace_string)

                nfs4acl.ace_add(ace_string, realpath.encode('utf-8'))

            else:
                raise Exception("Unknown permissions")

            fs = CouchedFileSystem(config.fs_mount_point, principal)
            current = os.path.dirname(filepath)
            while current != os.sep:
                fs.chmod(current, (fs[current].mode & 0777) | stat.S_IXOTH)
                current = os.path.dirname(current)

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

        finally:
            self.debug('End')

    @krb5principal
    def remove_permissions(self, meta, principal, participant, fileid):
        """
        delete permissions on the filepath for the participant
        @param owner       : the owner of the document refered by filepath
        @param participant : user who lose his permissions
        @param filepath : document
        """

        try:
            self.debug('Start')

            doc_helper = DocumentHelper(SyncDocument, principal)
            document = doc_helper.by_id(key=fileid, pk=True)
            filepath = os.path.join(document.dirpath, document.filename.decode('utf-8'))

            self.debug('Owner: %s, participant: %s, filepath: %s'
                       % (principal, participant, filepath.encode('utf-8')))

            if not self.is_owner(meta, fileid):
                raise BadOwnerError()

            realpath = os.path.join(config.fs_mount_point, filepath[1:])

            # Get the current ACL
            acl = nfs4acl.getfacl(realpath.encode('utf-8'))

            self.debug("---> acl: '%s'" % acl)

            # Get all informations about participant
            user = User(meta, participant)
            user.populate()

            # Delete the ACE giving access to the participant
            participant_with_domain = "%s@%s" % (participant, user.realm.lower())
            for ace in acl:
                if ace["who"] == participant_with_domain:
                    ace_to_delete = ace["type"] + ":" + ace["flags"] + ":" + ace["who"] + ":" + ace["access_mask"]
                    
                    nfs4acl.ace_del(ace_to_delete, realpath.encode('utf-8'))
                    self.debug("deleting the ACE '%s' from ACL's '%s'... OK" % (ace_to_delete, realpath.encode('utf-8')))

            fs = CouchedFileSystem(config.fs_mount_point, principal)
            share_helper = DocumentHelper(ShareDocument, principal)
            current = os.path.dirname(filepath)
            ids = set([ document.id ])
            while current != os.sep:
                documents = doc_helper.by_dir_prefix(key=current)
                for doc in [ doc for doc in documents if doc.id not in ids ]:
                    try:
                        share_helper.by_fileid(key=doc.id, pk=True)
                        return
                    except:
                        ids.add(doc.id)
                
                fs.chmod(current, (fs[current].mode & 0777) & ~stat.S_IXOTH)
                current = os.path.dirname(current)

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

        finally:
            self.debug('End')
