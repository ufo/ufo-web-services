#!/usr/bin/env python

from distutils.core import setup

setup(name='ufo-web-services',
      version='0.1',
      data_files=[ ('/usr/sbin', ['daemon/sync-spawner', 'daemon/quotadaemon', 'changes-monitor/ufo-changes-monitor']),
                   ('/etc/init.d', ['init/sync-spawner', 'init/quotadaemon', 'init/changes-monitor']) ],
      description='UFO servers components',
      author='Kevin Pouget',
      packages=['ufoaccount', 'ufosync', 'ufoserver', 'ufostorage', 'syncspawner', 'quotadaemon'],
      install_requires=['setuptools'],
)
