# Copyright (C) 2010  Agorabox. All Rights Reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from ipalib.rpc import KerbTransport
import xmlrpclib as rpc
from ipalib import api

import os

from ufoserver import config
from ufo.errors import *
from ufo.constants import FriendshipStatus, ShareDoc
from ufo.database import DocumentHelper
from ufo.debugger import Debugger
from ufo.utils import krb5principal, ComponentProxy
from ufo.notify import NewFriendshipNotification, \
                       AcceptedFriendshipNotification, \
                       CanceledFriendshipNotification
from ufoserver.user import User


class Account(Debugger):
    def __init__(self):
        """
        description ...
        """
        pass

    @krb5principal
    def get_user_infos(self, meta, principal):
        """
        Get the informations about the user user_name
        see also user.py
        """
        try:
            self.debug("Start with principal: %s" % principal)

            # create an instance of User
            user = User(meta, unicode(principal))

            # populate the object user with the informations 
            # fetched form ldap
            return user.populate()

        except Exception, e:
            self.debug_exception()
            raise

        finally:
            self.debug("End")

    @krb5principal
    def set_user_infos(self, meta, principal, other={}):
        """
        Set the user information. 
        user_name : login of the user that will be modified
        other        : set of concerned informations. This set must contain at least on
        of these key, otherwise your modifications do not take effect :
        {
        'first_name'=None,
        'last_name'=None,
        'home_directory'=None,
        'mail'=None,
        'userpassword'=None,
        'street'=None
        }
        Examples :
        set_user_info(u'raca', dict(last_name=u'my last name', street="my street"))
        """

        user = User(meta, unicode(principal))
        user.initialize(other)
        user.update()

    @krb5principal
    def invite_new_following(self, meta, principal, new_following):
        """
        description
        """
        try:
            self.debug("Start with principal: %s, new_following: %s" %
                        (principal, new_following))

            status = self.get_following_status(meta, new_following)
            if status == FriendshipStatus.BLOCKED_USER:
                raise BlockedUserError()

            elif status == FriendshipStatus.PENDING_FOLLOWING:
                raise PendingFollowingError()

            elif status == FriendshipStatus.FOLLOWING:
                raise AlreadyFollowingError()

            elif status == FriendshipStatus.NONE_FOLLOWING:
                # send an invitation to 'new_following' to informe him that
                # 'principal' wants to add him as a 'following'
                new_following_object = User(meta, unicode(new_following))
                new_following_object.add_pending_follower(unicode(principal))

                # adding 'new_following' as a 'pending_following'
                user_object = User(meta, unicode(principal))
                user_object.add_pending_following(unicode(new_following))

            else:
                raise BadFriendshipStatus()

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

    @krb5principal
    def invite_new_follower(self, meta, principal, new_follower):
        """
        description
        """
        try:
            self.debug("Start with principal: %s, new_following: %s" %
                        (principal, new_follower))

            status = self.get_follower_status(meta, new_follower)
            if status == FriendshipStatus.BLOCKED_USER:
                raise BlockedUserError()

            elif status == FriendshipStatus.PENDING_FOLLOWER:
                raise PendingFollowerError()

            elif status == FriendshipStatus.FOLLOWER:
                raise AlreadyFollowerError()

            elif status == FriendshipStatus.NONE_FOLLOWER:
                try:
                    # Add 'principal' as pending following of 'new_follower'
                    new_follower_object = User(meta, unicode(new_follower))
                    new_follower_object.add_pending_following(unicode(principal))

                except (PendingFollowingError, AlreadyFollowingError), e:
                    pass

                # Add 'new_follower' as pending follower of 'principal'
                user_object = User(meta, unicode(principal))
                user_object.add_pending_follower(unicode(new_follower))

                # Send a NewFriendship notification to 'new_follower',
                # to allow him to accept/refuse the invitation.
                notification_helper = DocumentHelper(NewFriendshipNotification, new_follower)
                notification_helper.create(following=principal, follower=new_follower)

            else:
                raise BadFriendshipStatus()

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

        finally:
            self.debug("End")

    @krb5principal
    def refuse_follower(self, meta, principal, pending_follower):
        try:
            self.debug("Start with principal: %s, pending_follower: %s" %
                        (principal, pending_follower))

            status = self.get_follower_status(meta, pending_follower)
            self.debug('status = %s' % status)

            if status == FriendshipStatus.BLOCKED_USER:
                raise BlockedUserError()

            elif status == FriendshipStatus.FOLLOWER:
                raise AlreadyFollowerError()

            elif status == FriendshipStatus.NONE_FOLLOWER:
                # FIXME : create a new exception for this error
                log_msg = "ERROR. You can not refuse a follower without invitation"
                self.debug(log_msg)
                raise Exception(log_msg)

            elif status == FriendshipStatus.PENDING_FOLLOWER:
                # Remove 'pending_follower' from the 'principal' friend list
                pending_follower_object = User(meta, unicode(pending_follower))
                pending_follower_object.remove_pending_following(unicode(principal))

                # Remove 'principal' from the 'pending_follower' friend list
                user_object = User(meta, unicode(principal))
                user_object.remove_pending_follower(unicode(pending_follower))

                # Send a CanceledFriendship notification to 'pending_follower'
                notification_helper = DocumentHelper(CanceledFriendshipNotification, pending_follower)
                notification_helper.create(following=principal)

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

    @krb5principal
    def refuse_following(self, meta, principal, pending_following):
        try:
            self.debug("Start with principal: %s, pending_following: %s" %
                        (principal, pending_following))

            status = self.get_following_status(meta, pending_following)
            self.debug('status = %s' % status)

            if status == FriendshipStatus.BLOCKED_USER:
                raise BlockedUserError()

            elif status == FriendshipStatus.FOLLOWING:
                raise AlreadyFollowerError()

            elif status == FriendshipStatus.NONE_FOLLOWING:
                # FIXME : create a new exception for this error
                log_msg = "ERROR. You can not refuse a following without invitation"
                self.debug(log_msg)
                raise Exception(log_msg)

            elif status == FriendshipStatus.PENDING_FOLLOWING:
                # Remove 'pending_follower' from the 'principal' friend list
                pending_following_object = User(meta, unicode(pending_following))
                pending_following_object.remove_pending_follower(unicode(principal))

                # Remove 'principal' from the 'pending_follower' friend list
                user_object = User(meta, unicode(principal))
                user_object.remove_pending_following(unicode(pending_following))

                # Send a CanceledFriendship notification to 'pending_follower'
                notification_helper = DocumentHelper(CanceledFriendshipNotification, pending_following)
                notification_helper.create(following=principal)

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

    @krb5principal
    def accept_follower(self, meta, principal, pending_follower):
        """
        description
        """
        try:
            self.debug("Start with principal: %s, pending_follower: %s" %
                        (principal, pending_follower))

            status = self.get_follower_status(meta, pending_follower)
            self.debug('status = %s' % status)

            if status == FriendshipStatus.BLOCKED_USER:
                raise BlockedUserError()

            elif status == FriendshipStatus.FOLLOWER:
                raise AlreadyFollowerError()

            elif status == FriendshipStatus.NONE_FOLLOWER:
                # FIXME : create a new exception for this error
                log_msg = "ERROR. You can not accept a follower without invitation"
                self.debug(log_msg)
                raise Exception(log_msg)

            elif status == FriendshipStatus.PENDING_FOLLOWER:
                # adding 'principal' as a 'following' for the 'pending_follower' user
                pending_follower_object = User(meta, unicode(pending_follower))
                pending_follower_object.add_following(unicode(principal), notify=False)

                # adding 'pending_follower' as a 'follower' for the 'principal'
                user_object = User(meta, unicode(principal))
                user_object.add_follower(unicode(pending_follower))

                # Send a AcceptedFriendship notification to 'principal',
                # to proceed the possible pending shares.
                notification_helper = DocumentHelper(AcceptedFriendshipNotification,
                                                     pending_follower)
                notification_helper.create(following=principal, follower=pending_follower)

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

    @krb5principal
    def accept_following(self, meta, principal, pending_following):
        """
        description
        """
        try:
            self.debug("Start with principal: %s, pending_following: %s" %
                        (principal, pending_following))

            status = self.get_following_status(meta, pending_following)

            self.debug('status = %s' % status)
            if status == FriendshipStatus.BLOCKED_USER:
                raise BlockedUserError()

            elif status == FriendshipStatus.FOLLOWING:
                raise AlreadyFollowingError()

            elif status == FriendshipStatus.NONE_FOLLOWING:
                # FIXME : create a new exception for this error
                log_msg = "ERROR. You can not accept a following without invitation"
                self.debug(log_msg)
                raise Exception(log_msg)

            elif status == FriendshipStatus.PENDING_FOLLOWING:
                # adding 'principal' as a 'following' for the 'pending_following' user
                pending_following_object = User(meta, unicode(pending_following))
                pending_following_object.add_follower(unicode(principal), notify=False)

                # adding 'pending_following' as a 'following' for the 'principal'
                user_object = User(meta, unicode(principal))
                user_object.add_following(unicode(pending_following))

                # Send a AcceptedFriendship notification to 'pending_following',
                # to proceed the possible pending shares.
                notification_helper = DocumentHelper(AcceptedFriendshipNotification,
                                                     pending_following)
                notification_helper.create(following=principal, follower=pending_following)

            else:
                raise BadFriendshipStatus()

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

    @krb5principal
    def remove_follower(self, meta, principal, follower):
        """
        description
        """
        try:
            self.debug("Start with principal: %s, follower: %s" % (principal, follower))

            status = self.get_follower_status(meta, follower)

            self.debug('Follower status = %s' % status)

            self.debug('Removing share of %s with %s if exists' % (principal, follower))
            remote_sync = ComponentProxy("ufosync.sync.Sync", config.sync_host, meta = meta)
            remote_sync.remove_shares_by_participant(follower)

            self.debug('Removing friends entries')
            user_obj = User(meta, unicode(principal))
            follower_obj = User(meta, unicode(follower))

            if status == FriendshipStatus.BLOCKED_USER:
                user_obj.remove_blocked_user(follower)
                follower_obj.remove_blocked_user(principal)

            if status == FriendshipStatus.FOLLOWER:
                user_obj.remove_follower(follower)
                follower_obj.remove_following(principal)

            elif status == FriendshipStatus.PENDING_FOLLOWER:
                user_obj.remove_pending_follower(follower)
                follower_obj.remove_pending_following(principal)

            elif status == FriendshipStatus.NONE_FOLLOWER:
                raise NotFollowerError()

            else:
                raise BadFriendshipStatus()

            # Send a CanceledFriendship notification to 'follower'
            notification_helper = DocumentHelper(CanceledFriendshipNotification, follower)
            notification_helper.create(following=principal)

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

    @krb5principal
    def get_follower_status(self, meta, principal, follower):
        """
        ...
        @return: 
        - FriendshipStatus.BLOCKED_USER :
              if 'user' is in the blocked users list of 'follower' or if
              'follower' is in the blocked users list of 'user'

        - FriendshipStatus.NONE_FOLLOWER :
              if neither 'follower' is in the 'pending followers' list
              nor in the 'followers' list of 'user'. We also do not have the
              BLOCKED_USER status. NONE_FOLLOWER status means that 'follower' can
              be a new follower of 'user'.

        - FriendshipStatus.PENDING_FOLLOWER :
              if 'follower' is in the 'pending followers' list of 'user'.

        - FriendshipStatus.FOLLOWER :
              if 'follower' is in the 'followers' list of 'user'.
        """

        try:
            self.debug("Start with principal: %s" % principal)

            # get all informations about follower
            follower_object = User(meta, unicode(follower))
            # populate the object user with the informations
            # fetched from ldap and database
            follower_object.populate()

            # we start with the blocked status because it is the priority
            if follower_object.has_blocked_user(principal):
                self.debug('return FriendshipStatus.BLOCKED_USER')
                return FriendshipStatus.BLOCKED_USER

            # get all informations about user
            user_object = User(meta, unicode(principal))
            # populate the object user with the informations 
            # fetched from ldap and database
            user_object.populate()

            self.debug("user_object.pending_followers : %s"
                       % user_object.pending_followers)
            self.debug("user_object.followers : %s"% 
                        user_object.followers)
            self.debug("user_object.pending_followings : %s"
                       % user_object.pending_followings)
            self.debug("user_object.followings : %s"
                       % user_object.followings)
            self.debug("user_object.blocked_users : %s"
                       % user_object.blocked_users)

            # we start with the blocked status because it is the priority
            if user_object.has_blocked_user(follower):
                self.debug('return FriendshipStatus.BLOCKED_USER')
                return FriendshipStatus.BLOCKED_USER

            elif (not user_object.has_pending_follower(follower)
                  and not user_object.has_follower(follower)):
                self.debug('return FriendshipStatus.NONE_FOLLOWER')
                return FriendshipStatus.NONE_FOLLOWER

            elif user_object.has_pending_follower(follower):
                self.debug('return FriendshipStatus.PENDING_FOLLOWER')
                return FriendshipStatus.PENDING_FOLLOWER

            elif user_object.has_follower(follower):
                self.debug('return FriendshipStatus.FOLLOWER')
                return FriendshipStatus.FOLLOWER
            else:
                raise BadFriendshipStatus()

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

        finally:
            self.debug("End")

    @krb5principal
    def get_following_status(self, meta, principal, following):
        """
        ...
        @return: 
        - FriendshipStatus.BLOCKED_USER :
              if 'user' is in the blocked users list of 'following' or if 
              'following' is in the blocked users list of 'user'

        - FriendshipStatus.NONE_FOLLOWING :
              if neither 'following' is in the 'pending followings' list
              nor in the 'followings' list of 'user'. We also do not have the
              BLOCKED_USER status. NONE_FOLLOWING status means that 'user' can
              invite 'following' to be a new following.

        - FriendshipStatus.PENDING_FOLLOWING :
              if 'following' is in the 'pending followings' list of 'user'.

        - FriendshipStatus.FOLLOWING :
              if 'following' is in the 'followings' list of 'user'.
        """
        try:
            self.debug("Start")

            if following == config.sync_public_user:
                self.debug("everyone is a friend of public")
                return FriendshipStatus.FOLLOWING

            # get all informations about following
            following_object = User(meta, unicode(following))
            # populate the object user with the informations 
            # fetched from ldap and database
            following_object.populate()

            # we start with the blocked status because it is the priority
            if following_object.has_blocked_user(principal):
                self.debug('return FriendshipStatus.BLOCKED_USER')
                return FriendshipStatus.BLOCKED_USER

            # get all informations about principal
            user_object = User(meta, unicode(principal))
            # populate the object user with the informations 
            # fetched from ldap and database
            user_object.populate()

            self.debug("user_object.pending_followers : %s" % 
                        user_object.pending_followers)
            self.debug("user_object.followers : %s"% 
                        user_object.followers)
            self.debug("user_object.pending_followings : %s" % 
                        user_object.pending_followings)
            self.debug("user_object.followings : %s"% 
                        user_object.followings)
            self.debug("user_object.blocked_users : %s"% 
                        user_object.blocked_users)

            # we start with the blocked status because it is the priority
            if user_object.has_blocked_user(following):
                self.debug('return FriendshipStatus.BLOCKED_USER')
                return FriendshipStatus.BLOCKED_USER

            elif (not user_object.has_pending_following(following)
                  and not user_object.has_following(following)):
                self.debug('return FriendshipStatus.NONE_FOLLOWING')
                return FriendshipStatus.NONE_FOLLOWING

            elif user_object.has_pending_following(following):
                self.debug('return FriendshipStatus.PENDING_FOLLOWING')
                return FriendshipStatus.PENDING_FOLLOWING

            elif not user_object.has_following(following):
                self.debug('return FriendshipStatus.FOLLOWING')
                return FriendshipStatus.FOLLOWING

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

        finally:
            self.debug("End")


class AccountAdmin(Account, Debugger):
    def __init__(self):
        """
        initiate the environment from which our methods will be called. 
        """
        super(AccountAdmin, self).__init__()

    def create_user(self, meta, user_name, first_name, last_name, realm, other={}):
        """
        Create a new user.
        user_name is the login of this user.
        other must be some thing like : 
        {
        'mail':'mymail@domain.tld',
        'street':'Avenue de Paris'
        }
        Examples of use :
        import xmlrpclib as rpc
        from ipalib.rpc import KerbTransport
        remote_accountadmin = ComponentProxy('ufoaccount.account.Account', meta,
                                             'https://ipa-server-f14.zeta.agorabox.org/xmlrpc', KerbTransport())
        remote_accountadmin.create_user('user_name',
        'first_name',
        'last_name',
        'GAMMA.AGORABOX.ORG',
        dict(mail='mail', 
        street='street')
        )
        
        """
        self.debug('Start')
        os.environ["KRB5CCNAME"] = meta['apache_env']['KRB5CCNAME']

        # create an instance of User
        user = User(meta, unicode(user_name))
        if other == None: 
            other = {}
        other['first_name'] = first_name
        other['last_name'] = last_name
        other['realm'] = realm

        # initialization
        user.initialize(other)
        # create the user on the ldap
        user.create()

        try:
            remote_sync = ComponentProxy("ufosync.sync.Sync", config.sync_host, meta = meta)
            remote_sync.init_storage(user_name, other.get("quota", 1024 * 1024))

        except Exception, e:
            # delete the ldap entry of this user
            user.delete()

            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

    def remove_user(self, meta, user_name):
        """
        Delete the user_name form ipa database (LDAP). The user directory is not 
        removed.
        """
        os.environ["KRB5CCNAME"] = meta['apache_env']['KRB5CCNAME']

        # delete the home directory of this user
        try:
            remote_sync = rpc.Server(config.sync_host, KerbTransport())
            remote_sync.sync.destroy_storage(user_name)

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

        # remove the user from ldap
        user.delete()
        
