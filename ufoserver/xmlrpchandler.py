from mod_python import apache
from xmlrpcserver import XmlRpcServer

def handler(req):
    try:
        # export the following classes
        xmlserver = XmlRpcServer()

        # Account Component
        try:
            from ufoaccount.account import Account, AccountAdmin
            account = Account()
            account_admin = AccountAdmin()
            xmlserver.register_class('account', account)
            xmlserver.register_class('account_admin', account_admin)
        except ImportError:
            pass
        
        # Sync Component
        try:
            from ufosync.sync import Sync
            sync = Sync()
            xmlserver.register_class('sync', sync)
        except ImportError:
            pass

        # Storage Component
        try:
            from ufostorage.storage import Storage
            storage = Storage()
            xmlserver.register_class('storage', storage)
        except ImportError:
            pass

        return xmlserver.handle(req, apache)

    except Exception, e:
        return apache.HTTP_INTERNAL_SERVER_ERROR
        
