# Copyright (C) 2010  Agorabox. All Rights Reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os

import xmlrpclib as rpc

from ufoserver import config
from ufo.errors import *
from ufo.constants import FriendshipStatus, Notification, ShareDoc
from ufo.database import DocumentHelper, DocumentException, ChangesSequenceDocument
from ufo.filesystem import SyncDocument
from ufo.sharing import ShareDocument, FriendDocument
from ufo.debugger import Debugger
from ufo.notify import NewShareNotification, CanceledShareNotification, NotificationDocument
from ufo.utils import fault_to_exception, krb5principal, ComponentProxy
from ufo.views import SortedByTypeSyncDocument, BuddySharesSyncDocument, MySharesSyncDocument, TaggedSyncDocument

class Sync(Debugger):

    def __init__(self):
        pass

    def init_storage(self, meta, user, quota):
        """
        Create a new storage directory for the user, with 'uid' 
        as user identifier, on the "Storage" entity.
        """

        try:
            self.debug("Start with user: %s" % user)

            storage_remote = ComponentProxy("ufostorage.storage.Storage",
                                            config.storage_host, meta = meta)

            # Create the home directory on the remote filesystem.
            storage_remote.mk_home_dir(user)
            storage_remote.set_quota(user, quota)

            # Here, perhaps add some customization stuff like adding new friend
            # like Mme Michu.

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

        finally:
            self.debug("End")

    def destroy_storage(self, meta, user):
        """
        Remove the storage directory on the "Storage"
        entity.
        """
        try:
            self.debug("Start with storage_directory = %s" % storage_directory)

            storage_remote = ComponentProxy("ufostorage.storage.Storage",
                                            config.storage_host, meta = meta)

            # remove the storage directory
            # force = True : ignore errors
            storage_remote.rm_home_dir(user, force=True)

            # Here, perhaps delete the user database, of delete shares
            # in it's friend databases.

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

        finally:
            self.debug("End")

###############################################################
#     Sharing and synchronization methods
###############################################################

    @krb5principal
    def _share_doc(self, meta, principal, participant, fileid, permissions):
        """
        description ...
        """
        try:
            self.debug("Start with principal = '%s', participant = '%s', document = '%s', permissions = '%s'"
                        % (principal, participant, id, permissions))

            storage_remote = ComponentProxy("ufostorage.storage.Storage",
                                            config.storage_host, meta = meta)

            # Add the new permissions to the file on the filesystem
            storage_remote.add_permissions(participant, fileid, permissions)

            sync_heper = DocumentHelper(SyncDocument, principal)
            doc = sync_heper.by_id(key=fileid, pk=True)

            # TODO: improve replicate usage of DocumentHelper...
            # TODO: replication by doc_ids does not handle deletion, need more investigation.
            self.debug("Replicating document %s -> %s, %s" % (principal, participant, fileid))
#            sync_heper.replicate("http://localhost:5984/%s" % participant, doc_ids=[fileid])
            DocumentHelper(SyncDocument, participant).create(**doc._data)

            # Replicating directories in the path if not exists.
            # TODO: Improve the query to get all directory in the same request
            #       than the document himself, in a function replicate_path
            current = os.path.dirname(os.path.join(doc.dirpath, doc.filename))
            while current != os.sep:
                try:
                    dir = sync_heper.by_path(key=current, pk=True)
                    DocumentHelper(SyncDocument, participant).create(**dir._data)

                except ConflictError, e:
                    pass

                finally:
                    current = os.path.dirname(current)

            # Add a share entry to the participant database.
            DocumentHelper(ShareDocument, participant).create(provider=principal,
                                                              participant=participant,
                                                              fileid=fileid,
                                                              permissions=permissions,
                                                              flags=ShareDoc.ENABLED_SHARE_FLAG,
                                                              notification=Notification.NOTIFY)

            # Send a NewShare notification to 'participant'
            notification_helper = DocumentHelper(NewShareNotification, participant)
            notification_helper.create(following=principal,
                                       filepath=os.path.join(doc.dirpath, doc.filename.decode('utf-8')))

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

        finally:
            self.debug("End")

    def _check_ownership(self, meta, fileid):
        """
        Description
        """
        try:
            storage_remote = ComponentProxy("ufostorage.storage.Storage",
                                            config.storage_host, meta = meta)
            return storage_remote.is_owner(fileid)
        except rpc.Fault, f:
            raise fault_to_exception(f)

    def _is_shared(self, provider, participant, fileid):
        """
        Description
        """
        key = [provider, participant, fileid]
        try:
            helper = DocumentHelper(ShareDocument, provider)
            helper.by_provider_and_participant_and_fileid(key=key,pk=True)
        except DocumentException, e:
          return False

        return True

    @krb5principal
    def add_new_share(self, meta, principal,  participant, fileid, permissions):
        """
        Description
        """
        try:
            self.debug("Start with principal = '%s', participant = '%s',"
                       " fileid = '%s', permissions = '%s'"
                       % (principal, participant, fileid, permissions))

            # Check if principal is the owner of document
            if not self._check_ownership(meta, fileid):
                raise BadOwnerError()

            if self._is_shared(principal, participant, fileid):
                raise AlreadySharedDocError()

            # Get the friendship status
            try:
                account_remote = ComponentProxy("ufoaccount.account.Account",
                                                config.account_host, meta = meta)
                friendship_st = account_remote.get_follower_status(participant)
            except rpc.Fault, f:
                raise fault_to_exception(f)

            if friendship_st == FriendshipStatus.BLOCKED_USER:
                raise BlockedUserError()

            elif friendship_st == FriendshipStatus.FOLLOWER:
                # Share the document on the filesystem
                self._share_doc(meta, participant, fileid, permissions)

                # Add a active share entry in the database
                DocumentHelper(ShareDocument, principal).create(provider=principal,
                                                               participant=participant,
                                                               fileid=fileid,
                                                               permissions=permissions,
                                                               flags=ShareDoc.ENABLED_SHARE_FLAG)

            elif friendship_st in (FriendshipStatus.NONE_FOLLOWER, FriendshipStatus.PENDING_FOLLOWER):
                # Send an invitation if participant is'nt follower yet
                if friendship_st == FriendshipStatus.NONE_FOLLOWER:
                    try:
                        account_remote.invite_new_follower(participant)
                    except rpc.Fault, f:
                        raise fault_to_exception(f)

                # Add a pending share entry in the database
                DocumentHelper(ShareDocument, principal).create(provider=principal,
                                                               participant=participant,
                                                               fileid=fileid,
                                                               permissions=permissions,
                                                               flags=ShareDoc.PENDING_SHARE_FLAG)

        except (OSError, PrivateError), e:
            self.debug("Raising exception: %s, %s" % (type(e), e))
            raise

        except Exception, e:
            self.debug_exception()
            raise

        finally:
            self.debug("End")

    @krb5principal
    def remove_participant_from_share(self, meta, principal, participant, fileid):
        """
        Description
        """
        try:
            self.debug("Start with principal = '%s', participant = '%s', fileid = '%s'"
                       % (principal, participant, fileid))

            # Check if principal is the owner of document
            if not self._check_ownership(meta, fileid):
                raise BadOwnerError()

            # Get the friendship status
            account_remote = ComponentProxy("ufoaccount.account.Account",
                                            config.account_host, meta = meta)
            friendship_st = account_remote.get_follower_status(participant)

            if friendship_st == FriendshipStatus.BLOCKED_USER:
                self.debug("It is not normal that a 'BLOCKED_USER' friend is on the list of participants")

            elif friendship_st == FriendshipStatus.NONE_FOLLOWER:
                self.debug("It is not normal that a 'NONE_FOLLOWER' friend is on the list of participants")

            elif friendship_st == FriendshipStatus.PENDING_FOLLOWER:
                self.debug("It is not normal that a 'PENDING_FOLLOWER' blocked friend is on the list of participants")

            elif friendship_st == FriendshipStatus.FOLLOWER:
                pass

            # Firstly change permissions on the filesystem
            storage_remote = ComponentProxy("ufostorage.storage.Storage",
                                            config.storage_host, meta = meta)
            storage_remote.remove_permissions(participant, fileid)

            # Then remove the share from the both database
            key = [principal, participant, fileid]
            try:
              share_helper = DocumentHelper(ShareDocument, principal)
              share_helper.delete(share_helper.by_provider_and_participant_and_fileid(key=key, pk=True))

            except DocumentException, e:
              self.debug("Unable to remove the share document from the database of %s (%s)"
                         % (principal, str(e)))

            # Finally remove the document from the participant database
            try:
              sync_heper = DocumentHelper(SyncDocument, participant)
              doc = sync_heper.by_id(key=fileid, pk=True)

              filepath = os.path.join(doc.dirpath, doc.filename)
              sync_heper.delete(doc)

              # Send a CanceledShare notification to 'participant'
              notification_helper = DocumentHelper(CanceledShareNotification, participant)
              notification_helper.create(following=principal,
                                         filepath=filepath.decode('utf-8'))

            except DocumentException, e:
              self.debug("Unable to remove the document from the database of %s (%s)"
                         % (participant, str(e)))

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

        finally:
            self.debug("End")

    @krb5principal
    def list_participants_of_shared_doc(self, meta, principal, fileid):
        """
        Description
        """
        try:
            self.debug("Start with principal = '%s', fileid = '%s'" %
                       (principal, fileid))

            # check if principal is the owner of path
            if not self._check_ownership(meta, fileid):
                raise BadOwnerError()

            helper = DocumentHelper(ShareDocument, principal)
            return [ { 'provider'    : doc.provider,
                       'participant' : doc.participant,
                       'fileid'      : doc.fileid,
                       'permissions' : doc.permissions,
                       'flags'       : doc.flags }
                     for doc in helper.by_provider_and_fileid(key=[principal, fileid]) ]

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

        finally:
            self.debug("End")

    @krb5principal
    def remove_shares_by_participant(self, meta, principal, participant):
        """
        Remove all the shares done by principal with participant.
        """
        try:
            self.debug(("Start with principal = '%s', participant = '%s'") %
                        (principal, participant))

            # FIXME : find how to control the access to the current method
            # check if principal is the owner of document
            # if not self._check_ownership(principal, document):
            #     raise Exception("%s is not the owner of the document : %s"
            #                     % (principal, document))

            # remove matching shares
            storage_remote = ComponentProxy("ufostorage.storage.Storage",
                                            config.storage_host, meta = meta)

            share_prov_helper = DocumentHelper(ShareDocument, principal)
            share_part_helper = DocumentHelper(ShareDocument, participant)
            sync_heper = DocumentHelper(SyncDocument, participant)
            notification_helper = DocumentHelper(CanceledShareNotification, participant)

            for share in share_prov_helper.by_provider_and_participant(key=[principal, participant]):
                # Delete the participant from the list of participants 
                # of the document
                storage_remote.remove_permissions(share.participant, share.fileid)

                # Remove the document from the participant database
                try:
                    doc = sync_heper.by_id(key=share.fileid, pk=True)
                    filepath = os.path.join(doc.dirpath, doc.filename)

                    sync_heper.delete(doc)

                    # Send a CanceledShare notification to 'participant'
                    notification_helper.create(following=principal, filepath=filepath)

                except DocumentException, e:
                    self.debug("Unable to remove document %s from db %s: %s"
                               % (share.fileid, participant, str(e)))

                share_prov_helper.delete(share)

            # Delete share entry from the participant database (Should disappear...)
            for share in share_part_helper.by_provider_and_participant(key=[principal, participant]):
                share_part_helper.delete(share)

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

        finally:
            self.debug("End")

    @krb5principal
    def proceed_pending_shares(self, meta, principal, participant):
        """
        description...
        """
        try:
            self.debug("Start with principal = '%s', participant = '%s'"
                       % (principal, participant))

            share_helper = DocumentHelper(ShareDocument, principal)

            key = [principal, participant, ShareDoc.PENDING_SHARE_FLAG]
            for share in share_helper.by_provider_and_participant_and_flag(key=key):
                # Share the document on the filesystem
                self._share_doc(meta, participant, share.fileid, share.permissions)

                # Update the share entry on the database
                share.flags = ShareDoc.ENABLED_SHARE_FLAG
                share_helper.update(share)

        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

        finally:
            self.debug("End")

    @krb5principal
    def remove_pending_shares(self, meta, principal, participant):
        """
        description  ...
        """
        try:
            self.debug(("Start with principal = '%s', participant = '%s'") % 
                        (principal, participant))

            share_helper = DocumentHelper(ShareDocument, principal)

            key = [principal, participant, ShareDoc.PENDING_SHARE_FLAG]
            for share in share_helper.by_provider_and_participant_and_flag(key=key):
                # Remove the share entry from the database
                share_helper.delete(share)
                
        except Exception, e:
            if isinstance(e, PrivateError):
                self.debug("Raising exception: %s, %s" % (type(e), e))
            else:
                self.debug_exception()

            raise

        finally:
            self.debug("End")
