import os
from time import sleep
from multiprocessing import current_process, log_to_stderr, Process
from multiprocessing.managers import BaseManager
import subprocess
import random
import tempfile
import string
import pwd
from ufo.filesystem import CouchedFileSystem
import logging
from ufoserver import config
import krbV
import subprocess

logger = log_to_stderr()
logger.setLevel(logging.DEBUG)

BASE_PORT = 20300
BASE_ADDR = "127.0.0.1"

class QuotaDaemon(object):
  def get_quota(self, krb5ccname, username):
    # Extract the principal from the ticket cache
    ccfile = tempfile.NamedTemporaryFile(delete=False)
    ccfile.write(krb5ccname)
    ccfile.flush()

    try:
        ctx = krbV.default_context()
        ccache = krbV.CCache(name='FILE:' + ccfile.name, context=ctx)
        cprinc = ccache.principal()

        ac = krbV.AuthContext(context=ctx)
        ac.flags = krbV.KRB5_AUTH_CONTEXT_DO_SEQUENCE|krbV.KRB5_AUTH_CONTEXT_DO_TIME
        ac.rcache = ctx.default_rcache()

        # creds = (cprinc, krbV.Principal('krbtgt/%s@%s' % (cprinc.realm, cprinc.realm), context=ctx),
        #          (0, None), (0,0,0,0), None, None, None, None,
        #          None, None)
        # ocreds = ccache.get_credentials(creds)

        print "Credentials validated for user", cprinc.name

        initiator = cprinc.name.split('@')[0]
        if cprinc.name != config.nfsadmin_principal and initiator != username:
            raise Exception("%s is not allowed to call get_quota for user %s" % (initiator, username))

        env = os.environ
        env["EDITOR"] = "cat"
        p = subprocess.Popen(["/usr/sbin/edquota", "-u", username], stdout=subprocess.PIPE, env=env)
        output = p.communicate()[0]
        quota = output.split("\n")[-2].split()[1:]
        return map(int, quota)

    finally:
        os.unlink(ccfile.name)

  def set_quota(self, krb5ccname, username, quota):
    # Extract the principal from the ticket cache
    ccfile = tempfile.NamedTemporaryFile(delete=False)
    ccfile.write(krb5ccname)
    ccfile.flush()

    try:
        ctx = krbV.default_context()
        print ccfile, ccfile.name, dir(ccfile)
        ccache = krbV.CCache(name='FILE:' + ccfile.name, context=ctx)
        cprinc = ccache.principal()
        sprinc = krbV.Principal(name=config.quota_daemon_principal, context=ctx)

        ac = krbV.AuthContext(context=ctx)
        ac.flags = krbV.KRB5_AUTH_CONTEXT_DO_SEQUENCE|krbV.KRB5_AUTH_CONTEXT_DO_TIME
        ac.rcache = ctx.default_rcache()

        (ac, req) = ctx.mk_req(server=sprinc, client=cprinc, auth_context=ac, ccache=ccache, options=krbV.AP_OPTS_MUTUAL_REQUIRED)
        """
        creds = (cprinc, krbV.Principal('krbtgt/%s@%s' % (cprinc.realm, cprinc.realm), context=ctx),
                 (0, None), (0,0,0,0), None, None, None, None,
                 None, None)
        ocreds = ccache.get_credentials(creds)
        """

        print "Credentials validated for user", cprinc.name

        if cprinc.name != config.nfsadmin_principal:
            raise Exception("%s is not allowed to call set_quota" % cprinc.name)

        # Convert megabytes into blocs
        quota = quota * 1024

        print "setting quota of %d to user %s" % (quota, username)
        os.system("quotatool -b -l %d -q 0 -u %s %s" % (quota, username, config.nfs_export)) 

    finally:
        os.unlink(ccfile.name)

class QuotaManager(BaseManager):
    pass

quotad = QuotaDaemon()

QuotaManager.register("quotad", callable=lambda: quotad)

